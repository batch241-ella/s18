console.log("Hello World!");

/*

*/

/*
	Syntax:
		function functionName(parameter) {
			code block
		}
		functionName(argument)
*/

// name is called parameter
// "parameter" acts as a named variable/container that exists only inside of a function
// it is used to store information that is provided to a function when it is called/invoked

function printName(name) {
	console.log("My name is " + name);
}

// "Juana" and "Imman", the information/data provided directly into the function, it is called an argument
printName("Juana");
printName("Imman");

// Variables can also be passed as an ARGUMENT
let myName = "Elaine";
printName(myName);

// Using Multiple Parameters
function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Erven", "Joshua", "Cabral");
// "Erven" will be stored in the parameter "firstName"
// "Joshua" will be stored in the parameter "middleName"
// "Cabral" will be stored in the parameter "lastName"

// In JS, providing more/less arguments than the expected parameters will not return an error
createFullName("Eric", "Andales"); // lastName is undefined
createFullName("Roland", "John", "Doroteo", "Jane"); // no error if more arguments than parameters

function checkDivisibiltyBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?")
	console.log(isDivisibleBy8);
}

checkDivisibiltyBy8(64);
checkDivisibiltyBy8(28);

/*
	Mini-Activity:
	1. Create a function which is able to receive data as an argument.
		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorite superhero in the console.
*/

function displayMyFavouriteSuperHero(superhero) {
	console.log("My favourite superhero is " + superhero);
}

displayMyFavouriteSuperHero("Homelander");

// Return Statement
/*
	The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
*/

function returnFullName(firstName, middleName, lastName) {
	return firstName + " " + middleName + " " + lastName;
	console.log("Can we print this message?");
}

// Whatever value that is returned from the "returnFullName" function can be stored in a variable
let completeName = returnFullName("John", "Doe", "Smith");

console.log(completeName);
console.log(returnFullName("John", "Doe", "Smith"));


function returnAddress(city, country) {
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Sta. Mesa, Manila", "Philippines");
console.log(myAddress);

// Consider the ff code

/*
	Mini-Activity:
	1. Debug our code so that the function will return a value and save it in a variable.
*/

function printPlayerInformation(userName, level, job) {
	console.log("Username: " + userName);
	console.log("Level: " + level);
	console.log("Job: " + job);

	return userName + " " + level + " " + job;
}

let user = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user); 


// function printPlayerInformation(userName, level, job) {
// 	console.log("Username: " + userName);
// 	console.log("Level: " + level);
// 	console.log("Job: " + job);
// }

// let user1 = printPlayerInformation("cardo123", "999", "Immortal");
// console.log(user1); //undefined

// returns undefined because printPlayerInformation returns nothing. It only logs the message in the console.
// You cannot save any value from printPlayerInformation() because it does not RETURN anything

