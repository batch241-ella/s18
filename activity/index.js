console.log("Hello World!");

let add = (a, b) => {
	console.log(`Displayed sum of ${a} and ${b}`)
	console.log(a + b);
}
let subtract = (a, b) => {
	console.log(`Displayed difference of ${a} and ${b}`)
	console.log(a - b);
}
add(5, 15);
subtract(20, 5);


let multiply = (a, b) => {
	console.log(`The product of ${a} and ${b}:`)
	return a * b;
}

let divide = (a, b) => {
	console.log(`The quotient of ${a} and ${b}:`)
	return a / b;
}

let product = multiply(50, 10);
console.log(product);
let quotient = divide(50, 10);
console.log(quotient);


let getCircleArea = (radius) => {
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return (Math.PI * Math.pow(radius, 2)).toFixed(2);
}

let circleArea = getCircleArea(15);
console.log(circleArea);


let getAverageOfFourNums = (num1, num2, num3, num4) => {
	console.log(`The average of ${num1}, ${num2}, ${num3}, and ${num4}`);
	return (num1 + num2 + num3 + num4) / 4;
}

let averageVar = getAverageOfFourNums(20, 40, 60, 80);
console.log(averageVar);


function checkIfPassed(score, totalScore) {
	let percentage = (score / totalScore) * 100;
	let isPassed = percentage >= 75;
	console.log("Is " + score + "/" + totalScore + " a passing score?");

	return isPassed;
}

let isPassingScore = checkIfPassed(38, 50);
console.log(isPassingScore);

